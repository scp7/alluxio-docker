### fuse and nfs

```
docker run --rm \
	-p 2049:2049 \
   --net=alluxio_network \
   --name=fuse-sp \
   --shm-size=1G \
   -v /tmp/mnt:/mnt \
   -e ALLUXIO_JAVA_OPTS=" \
      -Dalluxio.master.hostname=alluxio-master" \
   --cap-add SYS_ADMIN \
   --security-opt apparmor=unconfined \
   --device /dev/fuse \
   scp7/fuse-sp:0.1 fuse --fuse-opts=allow_other
   ```

vi exports

/mnt              *(rw,sync,fsid=0,crossmnt,no_subtree_check)
/mnt/alluxio-fuse *(rw,sync,fsid=1,no_subtree_check,insecure)

exportfs -a
rpcbind
service nfs-kernel-server restart

mount -t nfs4 -o nfsvers=4 -v localhost:/ /tmp/sp

