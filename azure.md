#### Using Azure Cloud Shell

```
ACI_PERS_RESOURCE_GROUP=cloud-shell-storage-westeurope
ACI_PERS_STORAGE_ACCOUNT_NAME=csb10032000d8658f8d
ACI_PERS_LOCATION=westeurope
ACI_PERS_SHARE_NAME=datashare
STORAGE_KEY=nk2WInB11WYlT6qp52S5TrjHd05PoJQiYWCeIfb66qeoc9oN09St+WVyS18PZwmj/CLkinfP8jTGvJH5pYodpA==
ACI_DOMAIN=.westeurope.azurecontainer.io
```

```
az container create \
    --resource-group $ACI_PERS_RESOURCE_GROUP \
    --name alluxio-master \
    --image docker.io/alluxio/alluxio \
    --dns-name-label aci-alluxio-master \
    --ports 19998 19999 \
    --azure-file-volume-account-name $ACI_PERS_STORAGE_ACCOUNT_NAME \
    --azure-file-volume-account-key $STORAGE_KEY \
    --azure-file-volume-share-name $ACI_PERS_SHARE_NAME \
    --azure-file-volume-mount-path /opt/alluxio/underFSStorage \
    --environment-variables 'ALLUXIO_JAVA_OPTS'='-Dalluxio.master.hostname=localhost -Dalluxio.master.mount.table.root.ufs=/opt/alluxio/underFSStorage' \
    --command-line "/entrypoint.sh master"
```

```
az container create \
    --resource-group $ACI_PERS_RESOURCE_GROUP \
    --name alluxio-worker \
    --image docker.io/alluxio/alluxio \
    --dns-name-label aci-alluxio-worker \
    --ports 29999 30000 \
    --memory 1 \
    --azure-file-volume-account-name $ACI_PERS_STORAGE_ACCOUNT_NAME \
    --azure-file-volume-account-key $STORAGE_KEY \
    --azure-file-volume-share-name $ACI_PERS_SHARE_NAME \
    --azure-file-volume-mount-path /opt/alluxio/underFSStorage \
    --environment-variables 'ALLUXIO_JAVA_OPTS'='-Dalluxio.master.hostname=aci-alluxio-master$ACI_DOMAIN -Dalluxio.worker.memory.size=1G' \
    --command-line "/entrypoint.sh worker"
```


