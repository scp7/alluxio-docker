# Alluxio Docker

Pages to get Alluxio working on Docker with Fuse and NFS. 

See [getting docker running on a VM in Azure](https://gitlab.com/scp7/alluxio-docker/-/blob/master/azure%20vm.md) then how to get the Alluxio docker images working with [Fuse on Azure]](https://gitlab.com/scp7/alluxio-docker/-/blob/master/fuse-on-azure-vm.md)

Dockerfiles and entrypoint.sh in [/working](https://gitlab.com/scp7/alluxio-docker/-/tree/master/working) directory