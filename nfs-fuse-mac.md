### Local Docker deployment of Alluxio on MacOS

#### Preparation
On MacOS, create a local directory for the underFSStorage

`mkdir /tmp/alluxio_ufs`

Create network in Docker for Master and Workers to communicate

`docker network create alluxio_network`

#### Master
```
docker run -d --rm \
   -p 19999:19999 \
   -p 19998:19998 \
	--net=alluxio_network \
	--name=alluxio-master \
	-v /tmp/alluxio_ufs:/opt/alluxio/underFSStorage \
	-e ALLUXIO_JAVA_OPTS=" \
       -Dalluxio.master.hostname=alluxio-master \
       -Dalluxio.master.mount.table.root.ufs=/opt/alluxio/underFSStorage" \
	alluxio/alluxio master
```


#### Worker
```
docker run -d --rm \
   -p 29999:29999 \
   -p 30000:30000 \
	--net=alluxio_network \
	--name=alluxio-worker \
	--shm-size=1G \
	-v /tmp/alluxio_ufs:/opt/alluxio/underFSStorage \
	-e ALLUXIO_JAVA_OPTS=" \
       -Dalluxio.worker.memory.size=1G \
       -Dalluxio.master.hostname=alluxio-master" \
   alluxio/alluxio worker
```

#### Worker with FUSE
```
docker run -d -P --rm \
   --net=alluxio_network \
   --name=alluxio-fuse \
   --shm-size=1G \
   -v /tmp/mnt:/mnt \
   -e ALLUXIO_JAVA_OPTS=" \
      -Dalluxio.master.hostname=alluxio-master" \
   --cap-add SYS_ADMIN \
   --device /dev/fuse \
   alluxio/alluxio-fuse fuse
```

#### Using exportFS to expose an NFS server on the fuse mount point
In this case we need to build a custom Fuse docker file - see Dockerfile.fuse - and then start the container passing in fuse option 'allow_other'. To get this to work the fuse.conf file must be copied to /etc/fuse.conf with the user_allow_other.


For NFS on mac to get kernel support need to extract modules.
```
docker run -P --rm \
   --net=alluxio_network \
   --name=alluxio-fuse \
   --shm-size=1G \
   -v /tmp/mnt:/mnt \
   -v /tmp/docker/lib/modules/modules:/lib/modules:ro \
   -e ALLUXIO_JAVA_OPTS=" \
      -Dalluxio.master.hostname=alluxio-master" \
   --cap-add SYS_ADMIN \
        --security-opt apparmor:unconfined \
   --device /dev/fuse \
   alluxio-fuse fuse --fuse-opts=allow_other
```

 

### Check this is functioning

#### Check web pages
Go to master web page: http://localhost:19999
Go to worker we page: http://localhost:30000


#### Run tests on worker

Connect to worker:
`docker exec -it alluxio-worker /bin/bash`

Use runTests:
```
cd /opt/alluxio
./bin/alluxio runTests
```

## Reference building an Alluxio fuse and nfs server on docker

Reference NFS server on docker implementations:
* https://github.com/ehough/docker-nfs-server
* https://github.com/ErezHorev/dockerized_nfs_server


On mac docker needs kernel support, see this issue:
https://github.com/ehough/docker-nfs-server/issues/26

Need to do the following:

In the meantime, here's what I would try:

```$ hdiutil attach /Applications/Docker.app/Contents/Resources/linuxkit/docker-desktop.iso```

This will mount a disk image at /Volumes/ISOIMAGE

```$  cp -r /Volumes/ISOIMAGE/lib/modules /tmp/docker/lib/modules```

You may unmount the disk image after this copy is done, if you'd like.

You must then share this folder with docker desktop:
* Preferences
* Resources > File Sharing
* Add: /tmp/docker/lib



Follow these instructions to allow the container to automatically load the required kernel modules. However instead of bind-mounting /lib/modules, you'll want to bind-mount whatever destination path you used in step 2. e.g.

```
docker run \
-p 2049:2049    \
--name=nfs-server \
--net=alluxio_network \
--privileged \
--cap-add SYS_ADMIN  \
--cap-add SYS_MODULE \
-e SHARED_DIRECTORY=/mnt -e NFS_LOG_LEVEL=DEBUG \
-v /tmp/mnt:/mnt \
-v /tmp/docker/lib/modules/modules:/lib/modules:ro \
-v "/Users/stephen/docker-dev/alluxiofusenfscontainer/docker-nfs-server/exports.txt":/etc/exports:ro \
erichough/nfs-server
```
