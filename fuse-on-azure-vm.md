# Alluxio Docker, Fuse and NFS Export
Fuse on VM with NFS Export.
* Although the export is RW, there appears to be an issue between the NFS server and Fuse layer on writes.
  

### Local Docker deployment of Alluxio on Azure VM
Check azure vm.md for getting docker working on Azure.  

#### Preparation

On Azure VM, create a local directory for the underFSStorage

`mkdir /tmp/alluxio_ufs`

Create network in Docker for Master and Workers to communicate

`docker network create alluxio_network`

#### Master
```
docker run -d --rm \
-p 19999:19999 \
-p 19998:19998 \
--net=alluxio_network \
--name=alluxio-master \
-v /tmp/alluxio_ufs:/opt/alluxio/underFSStorage \
-e ALLUXIO_JAVA_OPTS=" \
-Dalluxio.master.hostname=alluxio-master \
-Dalluxio.master.mount.table.root.ufs=/opt/alluxio/underFSStorage" \
alluxio/alluxio master
```

#### Worker
```
docker run -d --rm \
-p 29999:29999 \
-p 30000:30000 \
--net=alluxio_network \
--name=alluxio-worker \
--shm-size=1G \
-v /tmp/alluxio_ufs:/opt/alluxio/underFSStorage \
-e ALLUXIO_JAVA_OPTS=" \
-Dalluxio.worker.memory.size=1G \
-Dalluxio.master.hostname=alluxio-master" \
alluxio/alluxio worker
```

#### Worker with out of the box FUSE
Note FUSE needs privileges to mount, need --cap-add and also for Azure ---security-opt

    docker run -d -P --rm \
    --net=alluxio_network \
    --name=alluxio-fuse \
    --shm-size=1G \
    -v /tmp/mnt:/mnt \
    -e ALLUXIO_JAVA_OPTS=" \
    -Dalluxio.master.hostname=alluxio-master" \
    --cap-add SYS_ADMIN \
    --security-opt apparmor=unconfined \
    --device /dev/fuse \
    alluxio/alluxio-fuse fuse


#### Worker with custom FUSE and NFS export image
Need to read [https://help.ubuntu.com/community/NFSv4Howto](https://help.ubuntu.com/community/NFSv4Howto) for background.

On Azure VM need to install nfs and nfsd modules:

    modprobe nfs
    modprobe nfsd

For NFS Export will use NFS4 to perform a bind across Alluxio fuse mount to target nfs share. NFS export requires allow_other access to the fuse_mount, so must copy in a fuse.conf with this access set and pass in fuse option allow_other at run time.

Check out working directory. Perform a docker build and then a run.

    docker build -f Dockerfile.fuse -t scp7/fuse-nfs:0.1 .

and then run:

    docker run --rm \
    	-p 111:111 \
    	-p 2049:2049 \
       --net=alluxio_network \
       --name=fuse-sp \
       --shm-size=1G \
       -v /tmp/mnt:/mnt \
       -e ALLUXIO_JAVA_OPTS=" \
          -Dalluxio.master.hostname=alluxio-master" \
       --cap-add SYS_ADMIN \
       --security-opt apparmor=unconfined \
       --device /dev/fuse \
       scp7/fuse-nfs:0.1 fuse --fuse-opts=allow_other

Ports 2049 for NFS4 and ports 111 for rpc to advertise for showmount. If showmount is not needed this port doesn't need to be open.

Exec into the running fuse mount to check NFS exports and fuse mount is working correctly:

    docker exec -u 0 -it fuse-sp bash

Check `/etc/exports`and `/mnt/alluxio`

Also check if you can mount from within the container:

    mkdir -p /tmp/tmpmount
    mount -v -t nfs4 localhost:/ /tmp/tmpmount

Note with NFS4, you don't specify the complete mount path as it is relative to the root.

Use docker inspect to get the address of the fuse container as you'll need this address below.

Test from a remote container using pre-built image with nfs libraries but run interactively with different entrypoint.

    docker run -it --rm \
    --name nfs-client \
    --net=alluxio_network \
    --cap-add SYS_ADMIN \
    --security-opt apparmor=unconfined \
    --entrypoint /bin/sh \
    prologic/nfs-client

At the shell of the nfs-client:

    # rpcbind
    # mkdir -p /tmp/fs
    # showmount -e <fuse container IP>
    Export list for 172.18.0.4:
    /mnt/nfs-share *
    # mount -v -t nfs4 <fuse container IP>:/ /tmp/fs


    
